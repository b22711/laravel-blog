@extends('layouts.app')
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
{{-- app.blade.php @yeild('content') --}}
@section('content')
    <div class="card">
        <div class="card-body">
            {{-- $post from PostController show function --}}
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains('user_id',Auth::id()))
                    <button type="submit" class="btn btn-danger">Unlike</button>
                    
                    @else
                    <button type="submit" class="btn btn-success">Like</button>
                    
                    @endif

                   
                </form>


                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Post a Comment
                </button>
  
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Post a Comment</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form method="POST" action="/posts/{{$post->id}}/comment">
                        <div class="modal-body">
                            
                            <div class="mb-3">
                                <label for="comment-content" class="commentContent">Content</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Post</button>
                        </div>
                        </form>
                            
                    </div>
                    </div>
                </div>

                
            @endif
            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body mt-3">
            <h2>Comments:</h2>
            
            <p>{{$post->comments}}</p>

            <p class="card-subtitle mb-3">Created at: {{$post->comments->content}}</p>

        </div>
      </div>
@endsection