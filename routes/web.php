<?php

use Illuminate\Support\Facades\Route;
// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// return resources > view > about.blade.php page
Route::get('/about', function () {
    return view('about');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherin a view to create a post that will be returned to the user (form for creating a post).
// linkes to PostContoller file imported above, identifies class create
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts',[PostController::class, 'store']);

// define a route that will return a view containing all posts
Route::get('/posts',[PostController::class, 'index']);

// Activity
Route::get('/', [PostController::class,'welcome']);

// define a route that will return a view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPost']);

// define a route wherein a view showing a specific post with matching URL parameter $id to query for the database entry to be  shown.
// {id} to let laravel knows it is a unique id - can be accessed in the controller
Route::get('/posts/{id}',[PostController::class, 'show']);



// Activity
Route::get('/posts/{id}/edit',[PostController::class, 'edit']);

// define a route that will overwrite an existing post with the matching  url parameter ID via PUT method
Route::put('/posts/{id}',[PostController::class, 'update']);

// define a route that will delete a post of matching URL parameter ID
Route::delete('/posts/{id}',[PostController::class, 'archive']);

// define a route to unarchive a post
Route::put('/posts/{id}',[PostController::class, 'unarchive']);; 

// define a web route that will call the like action
Route::put('/posts/{id}/like',[PostController::class, 'likes']);

// define a web route that will call the comments action
Route::post('/posts/{id}/comment',[PostController::class, 'comments']);