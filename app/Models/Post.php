<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    //  establish post relationship to user (many to one)
    // when laravel retrieves a user it also retrieves all its posts
    // when a post is retrieved, the user (author) is also retrieved
    public function user () {
        return $this -> belongsTo('App\Models\User');
    }

    public function likes(){
        return $this->hasMany('App\Models\PostLike');
    }

    public function comments(){
        return $this->hasMany('App\Models\PostComment');
    }
}
