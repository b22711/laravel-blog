<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.
    public function create () {
        // refers to post folder under view then create file
        return view ('posts.create');
    }
    // action to receive form data and subsequently store said data in the posts table.
    public function store(Request $request){
        // check if a user is logged in
        if (Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // detects the user id of the logged in user (from imported Auth file)
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        } else {
            return redirect ('/login');
        }

    }

    // action that will return a view showing all posts
    public function index(){
        $posts = Post::where('isActive', 1)
        ->get();
        // $posts->all();
        return view('posts.index')->with('posts', $posts);
    }
    
    public function welcome(){
        // $posts = Post::all();
        // returns view welcom with post variable from $posts array

        // returns random posts
        // limit additional method - limits number of of posts get
        $posts = Post::inRandomOrder()
            ->limit(3)
            // get for viewing
            ->get();
        return view('welcome')->with('posts', $posts);
    } 

    // action for showing only the posts authored by authenticated user

    public function myPost(){
        if(Auth::user()){
            // gets posts of the logged in user
            $posts = Auth::user()->posts;

            // returns posts of logged in user
            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown

    public function show($id){
        $post = Post::find($id);
        
        return view('posts.show')->with('post', $post);
 
    }

    // Activity edit action

    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);
            
            return view('posts.edit')->with('post', $post);
        }
        else {
            return redirect('/login');
        }
 
    }

    // request - get body of form, id of the post
    public function update(Request $request, $id){
        // finds the post to be updated
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            // updates the $post fields based on the $req from the input fields
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            $post->save();
        } 
        
        return redirect('/posts');
        
    }

    public function destroy($id){
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }

    // archive a post
    public function archive($id){
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
        }

        $post->save();
        return redirect('/posts');
    }

    // unarchive a post
    public function unarchive($id){
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
        }

        $post->save();
        return redirect('/posts');
    }

    // action that will allow a authenticated user who is not the post author to toggle a like on the post being viewed

    public function likes($id){
        $post = Post::find($id);
        // To check if there is a logged in user
        if(Auth::user()){
            $user_id = Auth::user()->id;
            // if authenticated user is not the post author
            if ($post->user_id != $user_id) {
                // check if a post like has been made
                if($post->likes->contains("user_id",$user_id)){
                    PostLike::where('post_id',$post->id)->where('user_id', $user_id)->delete();
                } else {
                    // create a new like record to like this post
                    // instantiate a new PostLike object from the PostLike model
                    $postLike = new Postlike;
                    // define the properties of the $postLike object
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;

                    // save the postlike object in the database
                    $postLike->save();
                }

                return redirect("/posts/$id");
            }
            
        } else {
            return redirect("/login");
        }
    }

    // action that will allow a authenticated user who is not the post author to leave a comment on the post being viewed

    public function comments(Request $request,$id){
        $post = Post::find($id);
        // To check if there is a logged in user
        if(Auth::user()){
            $user_id = Auth::user()->id;
            // if authenticated user is not the post author
            if ($post->user_id != $user_id) {
                
                    // create a new comment record for this post
                    // instantiate a new PostLike object from the PostLike model
                    $postComment = new Comment;
                    $postComment->content = $request->input('content');
                    // detects the user id of the logged in user (from imported Auth file)
                    $postComment->user_id = (Auth::user()->id);

                    $postComment->save();
                

            }
            return redirect("/posts/$id");

            
        } else {
            return redirect("/login");
        }
    }
}
